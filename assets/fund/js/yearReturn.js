﻿var Samples = {};
var chartColors = {
    red: "rgb(255, 99, 132)",
    orange: "rgb(255, 159, 64)",
    yellow: "rgb(255, 205, 86)",
    green: "rgb(75, 192, 192)",
    blue: "rgb(54, 162, 235)",
    purple: "rgb(153, 102, 255)",
    grey: "rgb(201, 203, 207)"
};
var Months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
];

Samples.utils = {
    // Adapted from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
    srand: function (seed) {
        this._seed = seed;
    },

    rand: function (min, max) {
        var seed = this._seed;
        min = min === undefined ? 0 : min;
        max = max === undefined ? 1 : max;
        this._seed = (seed * 9301 + 49297) % 233280;
        return min + (this._seed / 233280) * (max - min);
    }
};

function randomScalingFactor() {
    return Math.round(Samples.utils.rand(-200, 800));
};

Samples.utils.srand(Date.now());

var ydata = [];
var inv = 10000;

for (var i = 0; i < 17; i++) {
    ydata.push(inv);
    inv = inv + randomScalingFactor();

}

var xdata = [];

var year = 2012;
var mon = 0;
for (var i = 0; i < 17; i++) {

    var str = year + "-" + Months[mon].substring(0,3);

    xdata.push(str);
    if (mon === 11) {
        year++;
        mon = 0;
    } else if (mon === 5) {
        mon = 11;
    } else {
        mon = 5;
    }
}

function getyearReturnGraph() {

    var config = {
        type: "bar",
        data: {
            labels: ["2017","2016","2015","2014","2013","2012"],//["2013", "2014", "2015", "2016", "2017", ""],
            datasets: [{
                borderColor: window.chartColors.green,
                data: [
                    1,
                    6.730,
                    3.649,
                    11.470,
                    14.130
                ],
                backgroundColor: ["black",window.chartColors.green, window.chartColors.green, window.chartColors.green, window.chartColors.green, window.chartColors.green],
                fill: false
            }]
        },
        options: {
            responsive: true,
            legend: {
                display: false
            },
            title: {
                display: false,
                text: "Hypothetical growth of USD 10,000 in 5 yrs. when invested with PIXO Advantage"
            },
            tooltips: {
                intersect: false,
                mode: "x",
                displayColors:false,
                callbacks: {
                    label: function(tooltipItem, data) {
                        console.log(tooltipItem + " || " + data);

                        var dataset = data.datasets[tooltipItem.datasetIndex];

                        var currentValue = dataset.data[tooltipItem.index];

                        var currentLabel = data.labels[tooltipItem.index];
                        if (currentValue === 1) return "";

                        return  currentValue + "%";
                    }
                }
            },
            hover: {
                intersect: false,

            },
            scales: {
                xAxes: [{
                    display: false,
                    ticks: {
                        autoSkip: true
                    },
                    scaleLabel: {
                        display: false
                    }
                }],
                yAxes: [{
                    display: false,
                    position: "right",
                    scaleLabel: {
                        display: false,
                        labelString: "USD"
                    },
                    ticks: {
                        callback: function (label) {
                            var val = parseInt(label);

                            return (val / 1000) + "k";
                            //var labelArray = label.split("|");
                            //return labelArray[0] === "10" && labelArray[1] === "2015" ? labelArray[2] : "";
                        }
                    }
                }]
            }
        }
    };
    var ctx = document.getElementById("yearReturn").getContext("2d");
    window.yearReturn = new Chart(ctx, config);
}
