﻿var chartColors = {
    red: "rgb(255, 99, 132)",
    orange: "rgb(255, 159, 64)",
    yellow: "rgb(255, 205, 86)",
    green: "rgb(75, 192, 192)",
    blue: "rgb(54, 162, 235)",
    purple: "rgb(153, 102, 255)",
    grey: "rgb(201, 203, 207)"
};

var randomScalingFactor = function () {
    return Math.round(Math.random() * 100);
};

function getDoughnut() {
    var cconfig = {
        type: "doughnut",
        data: {
            datasets: [{
                data: [
                    64.79,
                    4.74,
                    27.83,
                    2.64
                ],
                backgroundColor: [
                    window.chartColors.blue,
                    window.chartColors.orange,
                    window.chartColors.yellow,
                    window.chartColors.green

                ],
                label: "Dataset 1"
            }],
            labels: [
                "Domestic Equities",
                "International Equities",
                "Bonds",
                "Cash & Net Others Assets"
            ]
        },
        options: {
            responsive: true,
            legend: {
                display: false,
                position: "top"
            },
            title: {
                display: false,
                text: "Doughnut Chart"
            },
            animation: {
                animateScale: true,
                animateRotate: true
            },
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        console.log(tooltipItem + " || " + data);

                        var dataset = data.datasets[tooltipItem.datasetIndex];

                        var currentValue = dataset.data[tooltipItem.index];

                        var currentLabel = data.labels[tooltipItem.index];

                        return currentLabel + ": " + currentValue + "%";
                    }
                }
            }
        }
    };
    var chartarea = document.getElementById("chart-area").getContext("2d");
    window.myDoughnut = new Chart(chartarea, cconfig);
}