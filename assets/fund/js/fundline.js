﻿var Samples = {};
var chartColors = {
    red: "rgb(255, 99, 132)",
    orange: "rgb(255, 159, 64)",
    yellow: "rgb(255, 205, 86)",
    green: "rgb(75, 192, 192)",
    blue: "rgb(54, 162, 235)",
    purple: "rgb(153, 102, 255)",
    grey: "rgb(201, 203, 207)",
    darkgreen: "rgb(19, 158, 137)"
};
var Months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
];

Samples.utils = {
    // Adapted from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
    srand: function (seed) {
        this._seed = seed;
    },

    rand: function (min, max) {
        var seed = this._seed;
        min = min === undefined ? 0 : min;
        max = max === undefined ? 1 : max;
        this._seed = (seed * 9301 + 49297) % 233280;
        return min + (this._seed / 233280) * (max - min);
    }
};

function randomScalingFactor() {
    return Math.round(Samples.utils.rand(-200, 800));
};

Samples.utils.srand(Date.now());

var ydata1 = [];
var inv = 10000;

for (var i = 0; i < 17; i++) {
    ydata1.push(inv);
    inv = inv + randomScalingFactor();

}

var xdata1 = [];

var year = 2014;
var mon = 7;
for (var i = 0; i < 36; i++) {

    var str = Months[mon] + " " + year;

    xdata1.push(str);
    if (mon === 11) {
        year++;
        mon = 0;
    } else {
        mon ++;
    }

}
ydata1 = [100, 98.81, 102.54, 102.01, 107.94, 111.5, 111.75, 107.37, 109.55, 105.66, 106.4, 104.2, 103.71, 105.99, 105.18, 105.73, 102.66, 102.49, 102.66, 102.16, 106.49, 105.21, 111.42, 115.44, 116.09, 116.02, 115.86, 115.09, 118.57, 120.06, 124.49, 129.26, 130.51, 134.54, 137.52, 140.46];

function get3YearLineGraph() {
    var config = {
        type: "line",
        data: {
            labels: xdata1, //["2013", "2014", "2015", "2016", "2017", ""],
            datasets: [
                {
                    backgroundColor: window.chartColors.green,
                    borderColor: window.chartColors.darkgreen,
                    borderWidth: 2,
                    data: ydata1,
                    fill: true
                }
            ]
        },
        options: {
            maintainAspectRatio: false,
            legend: {
                display: false
            },
            title: {
                display: false,
                text: "Hypothetical growth of USD 10,000 in 5 yrs. when invested with PIXO Advantage"
            },
            tooltips: {
                mode: "x",
                intersect: false,
                displayColors: false,
                callbacks: {
                    label: function (tooltipItem, data) {

                        var dataset = data.datasets[tooltipItem.datasetIndex];

                        var currentValue = dataset.data[tooltipItem.index];

                        var currentLabel = data.labels[tooltipItem.index];

                        if (currentValue === 1) return "";

                        return "SGD "+ currentValue;
                    },
                    title: function (tooltipItems, data) {

                        var tooltipItem = tooltipItems[0];
                        var dataset = data.datasets[tooltipItem.datasetIndex];

                        var currentValue = dataset.data[tooltipItem.index];

                        var currentLabel = data.labels[tooltipItem.index];

                        if (currentValue === 1) return "";

                        return currentLabel;
                    }
                }
            },
            hover: {
                mode: "nearest",
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: false
                    },
                    ticks: {
                        autoSkip: true,
                        maxTicksLimit: 12,
                        callback: function (label) {
                            var val = label.split(" ");
                            return val[0].substr(0, 3) + " '" + val[1].replace("20", "");
                            return (val / 1000);
                            //var labelArray = label.split("|");
                            //return labelArray[0] === "10" && labelArray[1] === "2015" ? labelArray[2] : "";
                        }
                    }
                }],
                yAxes: [{
                    display: true,
                    position: "left",
                    scaleLabel: {
                        display: true,
                        labelString: "SGD"
                    },
                    ticks: {
                        autoSkip: true,
                        maxTicksLimit: 7,
                        callback: function (label) {
                            var val = parseInt(label);
                            return val;
                            return (val / 1000);
                            //var labelArray = label.split("|");
                            //return labelArray[0] === "10" && labelArray[1] === "2015" ? labelArray[2] : "";
                        }
                    }
                }]
            }
        }
    };
    var ctx = document.getElementById("line3year").getContext("2d");
    window.line3year = new Chart(ctx, config);
}
