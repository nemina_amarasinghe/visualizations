﻿var rainbow = ["Lower", "", "Heigher"];

$("#rainbow-slider")
    .slider({
        max: 2,
        min: 0,
        value: 1,
        disabled: true
    })
    .slider("pips", {
        rest: "label",
        labels: rainbow
    });

