﻿(function () {
    var width = document.getElementById('graph').offsetWidth,
        height = 750,
        h = height,
        rectWidth = 200,
        rectHeight = 16,
        S = 20,
        s = 8,
        R = 110,
        J = 30,
        paddingHeight = 15,
        paddingWidth = 10,
        interval = 1000,
        easeout = "elastic",
        heighlight = "#90A716",
        heighlight2 = "#CDE642",
        normcolor = "#8A9E14",
        countryColor = "#808080",
        conLabelColor = "#eeeeee",
        stLabelColor = "#eeeeee";

    var map, fullmap, x, j, H, A, P;

    var L = {},
        activeNode = {};
    var i, y;
    var r = d3.layout.tree().size([360, h / 2 - R]).separation(function (Y, X) {
        return (Y.parent === X.parent ? 1 : 2) / Y.depth;
    });
    var W = d3.svg.diagonal.radial().projection(function (X) {
        return [X.y, X.x / 180 * Math.PI];
    });
    var line = d3.svg.line().x(function (d) {
        return d[0];
    }).y(function (X) {
        return X[1];
    }).interpolate("bundle").tension(0.5);

    var graph = d3.select("#graph")
        .append("svg")
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    graph
        .append("rect")
        .attr("class", "bg").attr({
            x: width / -2,
            y: height / -2,
            width: width,
            height: height,
            fill: "transparent"
        }).on("click", onclick);

    var links = graph.append("g").attr("class", "links"),
        funds = graph.append("g").attr("class", "funds"),
        nodes = graph.append("g").attr("class", "nodes");

    var gInfo = d3.select("#graph-info");

    d3.json("./data/concept.json", function (error, data) {

        map = d3.map(data);

        fullmap = d3.merge(map.values());

        x = {};
        A = d3.map();

        fullmap.forEach(function (d) {
            d.key = name(d.name);
            d.canonicalKey = d.key;
            x[d.key] = d;
            if (d.group) {
                if (!A.has(d.group)) {
                    A.set(d.group, []);
                }
                A.get(d.group).push(d);
            }
        });
        j = d3.map();
        map.get("funds").forEach(function (aa) {
            aa.links = aa.links.filter(function (ab) {
                return typeof x[name(ab)] !== "undefined" && ab.indexOf("r-") !== 0;
            });
            j.set(aa.key, aa.links.map(function (ab) {
                var ac = name(ab);
                if (typeof j.get(ac) === "undefined") {
                    j.set(ac, []);
                }
                j.get(ac).push(aa);
                return x[ac];
            }));
        });
        var Z = window.location.hash.substring(1);
        if (Z && x[Z]) {
            fundsclick(x[Z]);
        } else {
            onclick();
            M();
        }
        window.onhashchange = function () {
            var aa = window.location.hash.substring(1);
            if (aa && x[aa]) {
                fundsclick(x[aa], true);
            }
        };
    });

    function onclick() {
        if (L.node === null) {
            return;
        }
        L = {
            node: null,
            map: {}
        };
        i = Math.floor(height / map.get("funds").length);
        y = Math.floor(map.get("funds").length * i / 2);
        map.get("funds").forEach(function (af, ae) {
            af.x = rectWidth / -2;
            af.y = ae * i - y;
        });
        var ad = 180 + J,
            Z = 360 - J,
            ac = (Z - ad) / (map.get("countries").length - 1);
        map.get("countries").forEach(function (af, ae) {
            af.x = Z - ae * ac;
            af.y = h / 2 - R;
            af.xOffset = -S;
            af.depth = 1;
        });
        ad = J;
        Z = 180 - J;
        ac = (Z - ad) / (map.get("styles").length - 1);
        map.get("styles").forEach(function (af, ae) {
            af.x = ae * ac + ad;
            af.y = h / 2 - R;
            af.xOffset = S;
            af.depth = 1;
        });
        H = [];
        var ab, Y, aa, X = h / 2 - R;
        map.get("funds").forEach(function (ae) {
            ae.links.forEach(function (af) {
                ab = x[name(af)];
                if (!ab || ab.type === "reference") {
                    return;
                }
                Y = (ab.x - 90) * Math.PI / 180;
                aa = ae.key + "-to-" + ab.key;
                H.push({
                    source: ae,
                    target: ab,
                    key: aa,
                    canonicalKey: aa,
                    x1: ae.x + (ab.type === "country" ? 0 : rectWidth),
                    y1: ae.y + rectHeight / 2,
                    x2: Math.cos(Y) * X + ab.xOffset,
                    y2: Math.sin(Y) * X
                });
            });
        });
        P = [];
        A.forEach(function (af, ag) {
            if (ag.length > 1) {
                var ae = (ag[0].x - 90) * Math.PI / 180;
                a2 = (ag[1].x - 90) * Math.PI / 180, bulge = 20;
                P.push({
                    x1: Math.cos(ae) * X + ag[0].xOffset,
                    y1: Math.sin(ae) * X,
                    xx: Math.cos((ae + a2) / 2) * (X + bulge) + ag[0].xOffset,
                    yy: Math.sin((ae + a2) / 2) * (X + bulge),
                    x2: Math.cos(a2) * X + ag[1].xOffset,
                    y2: Math.sin(a2) * X
                });
            }
        });
        window.location.hash = "";
        M();
    }

    function fundsclick(Y, X) {
        if (L.node === Y && X !== true) {
            if (Y.type === "fund") {
                    openFund(Y.name);
                //window.location.href = "/" + Y.slug;
                return;
            }
            L.node.children.forEach(function (aa) {
                aa.children = aa._group;
            });
            e();
            return;
        }
        if (Y.isGroup) {
            L.node.children.forEach(function (aa) {
                aa.children = aa._group;
            });
            Y.parent.children = Y.parent._children;
            e();
            return;
        }
        Y = x[Y.canonicalKey];
        fullmap.forEach(function (aa) {
            aa.parent = null;
            aa.children = [];
            aa._children = [];
            aa._group = [];
            aa.canonicalKey = aa.key;
            aa.xOffset = 0;
        });
        L.node = Y;
        L.node.children = j.get(Y.canonicalKey);
        L.map = {};
        var Z = 0;
        L.node.children.forEach(function (ac) {
            L.map[ac.key] = true;
            ac._children = j.get(ac.key).filter(function (ad) {
                return ad.canonicalKey !== Y.canonicalKey;
            });
            ac._children = JSON.parse(JSON.stringify(ac._children));
            ac._children.forEach(function (ad) {
                ad.canonicalKey = ad.key;
                ad.key = ac.key + "-" + ad.key;
                L.map[ad.key] = true;
            });
            var aa = ac.key + "-group",
                ab = ac._children.length;
            ac._group = [{
                isGroup: true,
                key: aa + "-group-key",
                canonicalKey: aa,
                name: ab,
                count: ab,
                xOffset: 0
            }];
            L.map[aa] = true;
            Z += ab;
        });
        L.node.children.forEach(function (aa) {
            aa.children = Z > 50 ? aa._group : aa._children;
        });
        window.location.hash = L.node.key;
        e();
    }

    function mouseout() {
        activeNode = {
            node: null,
            map: {}
        };
        onHover();
    }

    function mouseover(d) {
        if (activeNode.node === d) {
            return;
        }
        activeNode.node = d;
        activeNode.map = {};
        activeNode.map[d.key] = true;
        if (d.key !== d.canonicalKey) {
            activeNode.map[d.parent.canonicalKey] = true;
            activeNode.map[d.parent.canonicalKey + "-to-" + d.canonicalKey] = true;
            activeNode.map[d.canonicalKey + "-to-" + d.parent.canonicalKey] = true;
        } else {
            j.get(d.canonicalKey).forEach(function (e) {
                activeNode.map[e.canonicalKey] = true;
                activeNode.map[d.canonicalKey + "-" + e.canonicalKey] = true;
            });
            H.forEach(function (e) {
                if (activeNode.map[e.source.canonicalKey] && activeNode.map[e.target.canonicalKey]) {
                    activeNode.map[e.canonicalKey] = true;
                }
            });
        }
        onHover();
    }

    function M() {
        V();
        links.selectAll("path").attr("d", function (d) {
            return line([
                [d.x1, d.y1],
                [d.x1, d.y1],
                [d.x1, d.y1]
            ]);
        }).transition().duration(interval).ease(easeout).attr("d", function (d) {
            return line([
                [d.x1, d.y1],
                [d.target.xOffset * s, 0],
                [d.x2, d.y2]
            ]);
        });
        D(map.get("funds"));
        b(d3.merge([map.get("countries"), map.get("styles")]));
        C([]);
        m(P);
        gInfo.html('');
        mouseout();
        onHover();
    }

    function e() {
        var X = r.nodes(L.node);
        X.forEach(function (Z) {
            if (Z.depth === 1) {
                Z.y -= 20;
            }
        });
        H = r.links(X);
        H.forEach(function (Z) {
            if (Z.source.type === "fund") {
                Z.key = Z.source.canonicalKey + "-to-" + Z.target.canonicalKey;
            } else {
                Z.key = Z.target.canonicalKey + "-to-" + Z.source.canonicalKey;
            }
            Z.canonicalKey = Z.key;
        });
        V();
        links.selectAll("path").transition().duration(interval).ease(easeout).attr("d", W);
        D([]);
        b(X);
        C([L.node]);
        m([]);
        var Y = "";
        if (L.node.description) {
            Y = L.node.description;
        }
        gInfo.html(Y);
        mouseout();
        onHover();
    }

    function b(X) {
        var X = nodes.selectAll(".node").data(X, direction);
        var Y = X.enter().append("g")
            .attr("transform", function (d) {
                var Z = d.parent ? d.parent : {
                    xOffset: 0,
                    x: 0,
                    y: 0
                };
                return "translate(" + Z.xOffset + ",0)rotate(" + (Z.x - 90) + ")translate(" + Z.y + ")";
            }).attr("class", "node")
            .on("mouseover", mouseover)
            .on("mouseout", mouseout)
            .on("click", fundsclick);

        Y.append("circle").attr("r", 0);

        Y.append("text")
            .attr("stroke", "#fff")
            .attr("stroke-width", 4)
            .attr("class", "name-label-stroke");

        Y.append("text")
            .attr("font-size", 0)
            .attr("class", "name-label");

        Y.append("text")
            .attr("class", "mg");
        X.transition().duration(interval).ease(easeout).attr("transform", function (d) {
            if (d === L.node) {
                return null;
            }
            var aa = d.isGroup ? d.y + (7 + d.count) : d.y;
            return "translate(" + d.xOffset + ",0)rotate(" + (d.x - 90) + ")translate(" + aa + ")";
        });
        X.selectAll("circle").transition().duration(interval).ease(easeout).attr("r", function (d) {
            if (d === L.node) {
                return 100;
            } else {
                if (d.isGroup) {
                    return 7 + d.count;
                } else {
                    return 4.5;
                }
            }
        });
        X.selectAll("text.mg")
            .transition().duration(interval).ease(easeout).attr("dy", ".3em")
            .attr("font-size", function (d) {
                if (d.depth === 0) {
                    return 40;
                } else {
                    return 35;
                }
            }).text(function (d) {
                if (d.unicode) {
                    return d.unicode;
                } else {
                    return "";
                }
            })
            .attr("text-anchor", function (d) {
                if (d === L.node || d.isGroup) {
                    return "middle";
                }
                return d.x < 180 ? "start" : "end";
            }).attr("transform", function (d) {
                if (d === L.node) {
                    return null;
                } else {
                    if (d.isGroup) {
                        return d.x > 180 ? "rotate(180)" : null;
                    }
                }
                return d.x < 180 ? "translate(" + (paddingWidth + 90) + ")" : "rotate(180)translate(-" + (paddingWidth + 90) + ")";
            });




        X.selectAll("text.name-label").transition()
            .duration(interval)
            .ease(easeout)
            .attr("dy", ".3em")
            .attr("font-size", function (d) {
                if (d.depth === 0) {
                    return 20;
                } else {
                    return 15;
                }
            }).text(function (d) {
                return d.name;
            }).attr("text-anchor", function (Z) {
                if (Z === L.node || Z.isGroup) {
                    return "middle";
                }
                return Z.x < 180 ? "start" : "end";
            }).attr("transform", function (d) {
                if (d === L.node) {
                    return null;
                } else {
                    if (d.isGroup) {
                        return d.x > 180 ? "rotate(180)" : null;
                    }
                }
                return d.x < 180 ? "translate(" + paddingWidth + ")" : "rotate(180)translate(-" + paddingWidth + ")";
            });
        X.selectAll("text.name-label-stroke").attr("display", function (d) {
            return d.depth === 1 ? "block" : "none";
        });
        X.exit().remove();
    }

    function V() {
        var X = links.selectAll("path").data(H, direction);
        X.enter().append("path").attr("d", function (Z) {
            var Y = Z.source ? {
                x: Z.source.x,
                y: Z.source.y
            } : {
                x: 0,
                y: 0
            };
            return W({
                source: Y,
                target: Y
            });
        }).attr("class", "link");
        X.exit().remove();
    }

    function C(d) {
        var ac = graph.selectAll(".detail").data(d, direction);
        var Y = ac.enter().append("g").attr("class", "detail");
        var ab = d[0];
        if (ab && ab.type === "fund") {
            var aa = Y.append("a").attr("xlink:href", function (ae) {
                return "/" + ae.slug;
            });
            aa.append("text").attr("fill", heighlight).attr("text-anchor", "middle").attr("y", (paddingHeight + paddingWidth) * -1).text(function (ae) {
                return "#" + ae.episode;
            });
        } else {
            if (ab && ab.type === "country") {
                Y.append("text").attr("fill", normcolor).attr("text-anchor", "middle").attr("y", (paddingHeight + paddingWidth) * -1).text("COUNTRY");
                var ico = graph.selectAll(".node").data(d, direction);

                ico.selectAll("text.mg").transition()
                    .attr("font-size", 40)
                    .attr("fill", countryColor)
                    .attr("text-anchor", "middle")
                    .attr("transform", "translate(0,45)");

            } else {
                if (ab && ab.type === "style") {
                    var ad = ac.selectAll(".pair").data(A.get(ab.group).filter(function (ae) {
                        return ae !== ab;
                    }), direction);
                    ad.enter().append("text").attr("fill", normcolor).attr("text-anchor", "middle").attr("y", function (af, ae) {
                        return (paddingHeight + paddingWidth) * 2 + (ae * (paddingHeight + paddingWidth));
                    }).text(function (ae) {
                        return "(vs. " + ae.name + ")";
                    }).attr("class", "pair").on("click", fundsclick);
                    Y.append("text").attr("fill", normcolor).attr("text-anchor", "middle").attr("y", (paddingHeight + paddingWidth) * -1).text("STYLE");
                    ad.exit().remove();
                }
            }
        }
        ac.exit().remove();
        var X = graph.selectAll(".all-funds").data(d);
        X.enter().append("text").attr("text-anchor", "start").attr("x", width / -2 + paddingWidth).attr("y", height / 2 - paddingWidth).text("all funds").attr("class", "all-funds").on("click", onclick);
        X.exit().remove();
    }

    function D(Y) {
        var Y = funds.selectAll(".fund").data(Y, direction);
        var X = Y.enter().append("g").attr("class", "fund").on("mouseover", mouseover).on("mouseout", mouseout).on("click", fundsclick);

        X.append("rect")
            .attr("x", rectWidth / -2)
            .attr("y", rectHeight / -2)
            .attr("width", rectWidth)
            .attr("height", rectHeight).
            transition()
            .duration(interval)
            .ease(easeout)
            .attr("x", function (d) {
                return d.x;
            }).attr("y", function (d) {
                return d.y;
            });
        X.append("text").attr("x", function (d) {
            return rectWidth / -2 + paddingWidth;
        }).attr("y", function (d) {
            return rectHeight / -2 + paddingHeight;
        }).attr("fill", "#fff").text(function (d) {
            return d.name;
        }).transition().duration(interval).ease(easeout).attr("x", function (d) {
            return d.x + paddingWidth;
        }).attr("y", function (d) {
            return d.y + 10;
        });
        Y.exit().selectAll("rect").transition().duration(interval).ease(easeout).attr("x", function (Z) {
            return rectWidth / -2;
        }).attr("y", function (Z) {
            return rectHeight / -2;
        });
        Y.exit().selectAll("text").transition().duration(interval).ease(easeout).attr("x", function (Z) {
            return rectWidth / -2 + paddingWidth;
        }).attr("y", function (Z) {
            return rectHeight / -2 + paddingHeight;
        });
        Y.exit().transition().duration(interval).remove();
    }

    function m(Y) {
        var X = funds.selectAll("path").data(Y);
        X.enter().append("path").attr("d", function (d) {
            return line([
                [d.x1, d.y1],
                [d.x1, d.y1],
                [d.x1, d.y1]
            ]);
        }).attr("stroke", "#000").attr("stroke-width", 1.5).transition().duration(interval).ease(easeout).attr("d", function (d) {
            return line([
                [d.x1, d.y1],
                [d.xx, d.yy],
                [d.x2, d.y2]
            ]);
        });
        X.exit().remove();
    }

    function onHover() {

        funds.selectAll("rect").attr("fill", function (d) {
            return selectFill(d, "#000", heighlight, "#000");
        }).attr("transform", function (d) {
            return selectFill(d, "", "", "");
        });//.attr("style", "transition: all 200ms ease-in;transform: scale(1.5);");

        links.selectAll("path").attr("stroke", function (d) {
            if (d.source.type === "style" || d.target.type === "style")
                return selectFill(d, normcolor, heighlight2, normcolor);
            else
                return selectFill(d, normcolor, heighlight, normcolor);

        }).attr("stroke-width", function (d) {
            return selectFill(d, "1.5px", "4px", "1px");
        }).attr("opacity", function (d) {
            return selectFill(d, 0.4, 1, 0.3);
        }).sort(function (Y, X) {
            if (!activeNode.node) {
                return 0;
            }
            var aa = activeNode.map[Y.canonicalKey] ? 1 : 0,
                Z = activeNode.map[X.canonicalKey] ? 1 : 0;
            return aa - Z;
        });
        nodes.selectAll("circle").attr("fill", function (X) {
            if (X === L.node) {
                return "#000";
            } else {
                if (X.type === "country") {
                    return selectFill(X, "#666", heighlight, "#000");
                } else {
                    if (X.type === "style") {
                        return "#fff";
                    }
                }
            }
            return selectFill(X, "#000", heighlight, "#999");
        }).attr("stroke", function (X) {
            if (X === L.node) {
                return selectFill(X, null, heighlight, null);
            } else {
                if (X.type === "country") {
                    return "#000";
                } else {
                    if (X.type === "style") {
                        return selectFill(X, "#000", heighlight2, "#000");
                    }
                }
            }
            return null;
        }).attr("stroke-width", function (X) {
            if (X === L.node) {
                return selectFill(X, null, 2.5, null);
            } else {
                if (X.type === "country" || X.type === "style") {
                    return 1.5;
                }
            }
            return null;
        });
        nodes.selectAll("text.name-label").attr("fill", function (d) {
            return (d === L.node || d.isGroup) ? "#fff" :
                (d.type === "style") ?
                selectFill(d, stLabelColor, heighlight2, "#999") : selectFill(d, conLabelColor, heighlight, "#999");

        }).attr("font-size", function (d) {
            return selectFill(d, 14, 16, 14);
        });
        nodes.selectAll("text.mg").attr("fill", function (d) {
            return (d === L.node || d.isGroup) ? "#fff" : selectFill(d, countryColor, heighlight, "#999");
        }).attr("font-size", function (d) {
            return selectFill(d, 35, 50, 35);
        });;
    }

    function name(d) {
        return d.toLowerCase().replace(/[ .,()]/g, "-");
    }

    function direction(d) {
        return d.key;
    }

    function selectFill(d, defcolor, hicolor, norcolor) {
        if (activeNode.node === null) {
            return defcolor;
        }
        return activeNode.map[d.key] ? hicolor : defcolor;
    }

    function openFund(fname) {
        $(".fund-id").html(fname);
        $("#drawer-content").animate({ "margin-right": 0 }, "slow");
        $("#main").animate({"display": "none"},"slow");
        $(".overlaydark").show();
        $(".fdiv").addClass("leftslide");

        setTimeout(function() {
            getLineGraph();
            getDoughnut();
        }, 200);

    }
})();