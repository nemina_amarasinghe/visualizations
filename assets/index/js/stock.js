//# dc.js Getting Started and How-To Guide
'use strict';

/* jshint globalstrict: true */
/* global dc,d3,crossfilter,colorbrewer */

// ### Create Chart Objects

// Create chart objects associated with the container elements identified by the css selector.
// Note: It is often a good idea to have these objects accessible at the global scope so that they can be modified or
// filtered by other page controls.
var gainOrLossChart = dc.pieChart('#gain-loss-chart');
var moveChart = dc.lineChart('#monthly-move-chart');
var volumeChart = dc.barChart('#monthly-volume-chart');
var nasdaqCount = dc.dataCount('.dc-data-count');
var quarterChart = dc.rowChart('#day-of-week-chart');
var netWorthByContryChart = dc.barChart('#worth-by-country-chart');

var datatable = null;
// ### Anchor Div for Charts
/*
// A div anchor that can be identified by id
    <div id='your-chart'></div>
// Title or anything you want to add above the chart
    <div id='chart'><span>Days by Gain or Loss</span></div>
// ##### .turnOnControls()

// If a link with css class `reset` is present then the chart
// will automatically hide/show it based on whether there is a filter
// set on the chart (e.g. slice selection for pie chart and brush
// selection for bar chart). Enable this with `chart.turnOnControls(true)`

// dc.js >=2.1 uses `visibility: hidden` to hide/show controls without
// disrupting the layout. To return the old `display: none` behavior,
// set `chart.controlsUseVisibility(false)` and use that style instead.
    <div id='chart'>
       <a class='reset'
          href='javascript:myChart.filterAll();dc.redrawAll();'
          style='visibility: hidden;'>reset</a>
    </div>
// dc.js will also automatically inject the current filter value into
// any html element with its css class set to `filter`
    <div id='chart'>
        <span class='reset' style='visibility: hidden;'>
          Current filter: <span class='filter'></span>
        </span>
    </div>
*/

//### Load your data

//Data can be loaded through regular means with your
//favorite javascript library
//
//```javascript
//d3.csv('data.csv', function(data) {...});
//d3.json('data.json', function(data) {...});
//jQuery.getJson('data.json', function(data){...});
//```
d3.csv('./data/forbes2.csv', function (data) {

    //var data = csv.filter(function (d) {
    //   /return (d.NetWorth < 3000);
    //    return  (d.Country !== "United States");
    //});
    // Since its a csv file we need to format the data a bit.
    var numberFormat = d3.format('.2f');

    // Net worth range size
    var rangeSize = 500;
    var rangeGeneralSize = 100;

    var minNetWorth = d3.min(data, function(d) { return +d.NetWorth; });
    var maxNetWorth = d3.max(data, function(d) { return +d.NetWorth; });
    
    var rangeStart = Math.floor(minNetWorth);
    var rangeEnd = Math.ceil(maxNetWorth);

    data.forEach(function (d) {
        d.SRange = Math.floor(d.NetWorth / rangeSize) * rangeSize;

        /*
        d.dd = .parse(d.date);
        d.month = d3.time.month(d.dd); // pre-calculate month for better performance
        d.close = +d.close; // coerce to number
        d.open = +d.open;*/
    });

    //### Create Crossfilter Dimensions and Groups

    //See the [crossfilter API](https://github.com/square/crossfilter/wiki/API-Reference) for reference.
    var ndx = crossfilter(data);
    var all = ndx.groupAll();

    // Dimension by month
    var moveNetWorths = ndx.dimension(function (d) {
        return d.SRange;
    });

    var _added_months = [];
    // Group by total volume within move, and scale down result
    var volumeByNetWorthGroup = moveNetWorths.group().reduceSum(function (d) {
        var dtm = d.SRange;
        var ind = _added_months.indexOf(dtm);
        if (ind != -1) {
            return 0;
        }
        else {
            _added_months.push(dtm);
            return 1;
        }
    });
    
    // Group by total volume within move, and scale down result
    var volumeByNetWorthMaleGroup = moveNetWorths.group().reduceSum(function (d) {
        return d.Gender === "M" ? d.NetWorth : 0;
    });
    var volumeByNetWorthFemaleGroup = moveNetWorths.group().reduceSum(function (d) {
        return d.Gender === "F" ? d.NetWorth : 0;
    });

    // Summarize volume by quarter
    var quarter = ndx.dimension(function (d) {
        var df = d.SRange;
        var start = df;
        var end = df+rangeSize;
        var str = "";

        if (start < 1000)
            str = str + "$" + start + "M";
        else {
            str = str + "$" + start / 1000 + "B";

        }
        if (end < 1000)
            str = str + " to $" + (end) + "M";
        else
            str = str + " to $" + (end) / 1000 + "B";

        return str;

    });
    var quarterGroup = quarter.group();

    // Create categorical dimension
    var gainOrLoss = ndx.dimension(function (d) {
        return d.Gender;
    });
    // Produce counts records in the dimension
    var gainOrLossGroup = gainOrLoss.group();

    var country = ndx.dimension(function (d) {
        return d.Country;
    });

    var countryGroup = country.group().reduceSum(function (d) {
        return d.NetWorth;
    });
    
    var tech_data = countryGroup.top(Infinity);
    var tech_max = d3.max(tech_data, function (d) { return d.value; });
    var tech_min_visible = tech_max / 100;

    netWorthByContryChart /* dc.rowChart('#day-of-week-chart', 'chartGroup') */
        .group(countryGroup)
        .dimension(country)
        .valueAccessor(function (d) {
            return tech_min_visible > d.value ? Math.round(tech_min_visible / d.value) * d.value : d.value;
        })
        .width(990)
        .height(400)
        .margins({ top: 10, right: 0, bottom: 100, left: 70 })
        .x(d3.scale.ordinal().domain(data.map(function (d) {
            return d.Country;
        })))
        .xUnits(dc.units.ordinal)
        .elasticY(true)
        .renderHorizontalGridLines(true)
        .title(function (d) {
            return d.key + ": $" + Math.round(d.value);
        }).yAxis().tickFormat(function(v) {
            var val = Math.round(v);

            return "$" + (val > 1000? ((val/1000)+ "B") : val+ "M");
        });

    gainOrLossChart /* dc.pieChart('#gain-loss-chart', 'chartGroup') */
        // (_optional_) define chart width, `default = 200`
        .width(180)
        // (optional) define chart height, `default = 200`
        .height(180)
        // Define pie radius
        .radius(80)
        // Set dimension
        .dimension(gainOrLoss)
        // Set group
        .group(gainOrLossGroup)
        // (_optional_) by default pie chart will use `group.key` as its label but you can overwrite it with a closure.
        .label(function(d) {
            if (gainOrLossChart.hasFilter() && !gainOrLossChart.hasFilter(d.key)) {
                return d.key + '(0%)';
            }
            var label = d.key;
            if (all.value()) {
                label += '(' + Math.floor(d.value / all.value() * 100) + '%)';
            }
            return label;
        });
    var colorScale = d3.scale.ordinal().range(['#1f77b4 ']);

    quarterChart /* dc.pieChart('#quarter-chart', 'chartGroup') */
        .width(380)
        .height(180)
        .dimension(quarter)
        .colors(colorScale)
        .margins({ top: 20, left: 10, right: 10, bottom: 20 })
        .group(quarterGroup)
            .title(function (d) {
                //return d.value;
            })
        .elasticX(true)
        .xAxis().ticks(4);

    //#### Stacked Area Chart
    var xax = d3.scale.linear()
        .domain([0, rangeEnd])
        .range([rangeStart, rangeEnd]);

    xax.tickFormat(5, "+%");
        

    //xax.tickFormat(function(v) {
    //    var val = Math.round(v);
    //    console.log(val);
    //    return "$" + (val > 1000 ? ((val / 1000) + "B") : val + "M");
    //});
    //Specify an area chart by using a line chart with `.renderArea(true)`.
    // <br>API: [Stack Mixin](https://github.com/dc-js/dc.js/blob/master/web/docs/api-latest.md#stack-mixin),
    // [Line Chart](https://github.com/dc-js/dc.js/blob/master/web/docs/api-latest.md#line-chart)
    moveChart /* dc.lineChart('#monthly-move-chart', 'chartGroup') */
        .renderArea(true)
        .width(990)
        .height(200)
        .transitionDuration(1000)
        .margins({ top: 30, right: 50, bottom: 25, left: 100 })
        .dimension(moveNetWorths)
        .mouseZoomable(true)
        // Specify a "range chart" to link its brush extent with the zoom of the current "focus chart".
        .rangeChart(volumeChart)
        .x(xax)
        //.round(d3.time.month.round)
        //.xUnits(d3.time.months)
        .elasticY(true)
        .renderHorizontalGridLines(true)
        //##### Legend

        // Position the legend relative to the chart origin and specify items' height and separation.
        .legend(dc.legend().x(800).y(10).itemHeight(13).gap(5))
        .brushOn(false)
        // Add the base layer of the stack with group. The second parameter specifies a series name for use in the
        // legend.
        // The `.valueAccessor` will be used for the base layer
        .group(volumeByNetWorthMaleGroup, "M")
        // Stack additional layers with `.stack`. The first paramenter is a new group.
        // The second parameter is the series name. The third is a value accessor.
        .stack(volumeByNetWorthFemaleGroup, "F")
        // Title can be called by any stack layer.
        .title(function(d) {
            var value = d.value;
            if (isNaN(value)) {
                value = 0;
            }
            return (d.key + "M to " + (+d.key + 1 - 0.01)) + "M" + '\n $' + numberFormat(value);
        }).yAxis().ticks(6).tickFormat(function(v) {
            var val = Math.round(v);

            return "$" + (val > 1000 ? ((val / 1000) + "B") : val + "M");
        });


    //#### Range Chart

    // Since this bar chart is specified as "range chart" for the area chart, its brush extent
    // will always match the zoom of the area chart.
    volumeChart.width(990) /* dc.barChart('#monthly-volume-chart', 'chartGroup'); */
        .height(40)
        .margins({top: 0, right: 50, bottom: 20, left: 40})
        .dimension(moveNetWorths)
        .group(volumeByNetWorthGroup)
        .centerBar(true)
        .gap(1)
        .x(xax)
        //.round(d3.time.month.round)
        .alwaysUseRounding(true);
        //.xUnits(d3.time.months);


    function RefreshTable() {
        dc.events.trigger(function () {
            datatable.fnClearTable();
            var v = moveNetWorths.top(Infinity);
            if (v.length > 0) {
                datatable.fnAddData(v);
                datatable.fnDraw();
            }
        });
    }
    

    datatable = $("#dc-data-table").dataTable({
        "aaData": moveNetWorths.top(Infinity),
        "aoColumns": [
            {
                "mData": "ClientName", "sDefaultContent": ""
            },
            {
                "mData": "Age", "sDefaultContent": ""
            },
            {
                "mData": "Gender", "sDefaultContent": ""
            },
            {
                "mData": "Region", "sDefaultContent": ""
            },
            {
                "mData": "Country", "sDefaultContent": ""
            },
            {
                "mData": "NetWorth", "sDefaultContent": ""
            },
            {
                "mData": "FXBullion", "sDefaultContent": ""
            },
            {
                "mData": "CashEquivalents", "sDefaultContent": ""
            },
            {
                "mData": "Equity", "sDefaultContent": ""
            },
            {
                "mData": "FixedIncomeBonds", "sDefaultContent": ""
            },
            {
                "mData": "Funds", "sDefaultContent": ""
            },
            {
                "mData": "Loan", "sDefaultContent": ""
            },
            {
                "mData": "ResidentialMortgage", "sDefaultContent": ""
            },
            {
                "mData": "CreditCard", "sDefaultContent": ""
            }
        ]
    });

    for (var i = 0; i < dc.chartRegistry.list().length - 1; i++) {
        var chartI = dc.chartRegistry.list()[i];
        chartI.on("filtered", RefreshTable);
    }
    //#### Rendering

    nasdaqCount /* dc.dataCount('.dc-data-count', 'chartGroup'); */
        .dimension(ndx)
        .group(all)
        // (_optional_) `.html` sets different html when some records or all records are selected.
        // `.html` replaces everything in the anchor with the html given using the following function.
        // `%filter-count` and `%total-count` are replaced with the values obtained.
        .html({
            some: '<strong>%filter-count</strong> selected out of <strong>%total-count</strong> records' +
                ' | <a href=\'javascript:dc.filterAll(); dc.renderAll();\'>Reset All</a>',
            all: 'All records selected. Please click on the graph to apply filters.'
        });

    //simply call `.renderAll()` to render all charts on the page
    dc.renderAll();
    /*
    // Or you can render charts belonging to a specific chart group
    dc.renderAll('group');
    // Once rendered you can call `.redrawAll()` to update charts incrementally when the data
    // changes, without re-rendering everything
    dc.redrawAll();
    // Or you can choose to redraw only those charts associated with a specific chart group
    dc.redrawAll('group');
    */

});

//#### Versions

//Determine the current version of dc with `dc.version`
//d3.selectAll('#version').text(dc.version);

// Determine latest stable version in the repo via Github API
//d3.json('https://api.github.com/repos/dc-js/dc.js/releases/latest', function (error, latestRelease) {
    /*jshint camelcase: false */
    /* jscs:disable */
  //  d3.selectAll('#latest').text(latestRelease.tag_name);
//});
