$(function() {
    function ratingEnable() {


        $('#returns').barrating('show', {
            theme: 'bars-movie',
            showSelectedRating: false,
            readonly: true
        });


        $('#expenses').barrating('show', {
            theme: 'bars-movie',
            showSelectedRating: false,
            readonly: true
        });


        $('#overall').barrating({
            theme: 'css-stars',
            showSelectedRating: false,
            readonly: true
        });

    }

    ratingEnable();
});
