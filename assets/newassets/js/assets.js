﻿// Breadcrumb dimensions: width, height, spacing, width of tip/tail.
    var b = {
        w: 75,
        h: 30,
        s: 3,
        t: 10
    };

// Dimensions of sunburst.
var width = document.getElementById('chart').clientWidth;
var height = document.getElementById('chart').clientHeight;
var radius = Math.min(width, height) / 2;
var perctop = height / 2 - b.h;
var percleft = width / 2 - b.w;

length = 100;

// Mapping of step names to colors.
var names = [];


var partition = d3.layout.partition()
    .size([2 * Math.PI, radius * radius])
    .value(function(d) { return d.size; });


// Use d3.text and d3.csv.parseRows so that we do not need to have a header
// row, and can receive the csv as an array of arrays.
d3.json("./data/assets.json", function(json) {
    //var csv = d3.csv.parseRows(text);
    //var json = buildHierarchy(csv);
    names = [];
    var colors = {};
    var color = d3.scale.linear().domain([1, length])
        .interpolate(d3.interpolateHcl)
        .range([d3.rgb("#D29700"), d3.rgb('#6200BA')]);

    getNames(json.children);

    var i = length / names.length;

    for (var j = 0; j < names.length; j++) {
        colors[names[j]] = color(j * i);
    }


    var config = {
        json: json,
        colors: colors,
        chart: "#chart",
        togglelegend: "#togglelegend",
        container: "#container",
        sequence: "#sequence",
        legend: "#legend",
        explanation: "#explanation",
        nodename: "#node-name",
        percentage: "#percentage",
        trail: "trail",
        endlabel: "endlabel",
        perctop: perctop + "px",
        percleft: percleft + "px"
    };

    var vis = d3.select(config.chart)
        .append("svg:svg")
        .attr("width", width)
        .attr("height", height)
        .append("svg:g")
        .attr("id", "container")
        .attr("transform", "translate(" + (width / 2) + "," + height / 2 + ")");


    intializeVisualization(vis, config).init();
});

d3.json("./data/liabilities.json", function(json) {
    //var csv = d3.csv.parseRows(text);
    //var json = buildHierarchy(csv);
    names = [];
    var colors = {};


    var color = d3.scale.linear().domain([1, length])
        .interpolate(d3.interpolateHcl)
        .range([d3.rgb("#3400BA"), d3.rgb('#00D2B7')]);

    getNames(json.children);

    var i = length / names.length;

    for (var j = 0; j < names.length; j++) {
        colors[names[j]] = color(j * i);
    }

    var config = {
        json: json,
        colors: colors,
        chart: "#chart1",
        togglelegend: "#togglelegend1",
        container: "#container1",
        sequence: "#sequence1",
        legend: "#legend1",
        explanation: "#explanation1",
        nodename: "#node-name1",
        percentage: "#percentage1",
        trail: "trail1",
        endlabel: "endlabel1",
        perctop: perctop + "px",
        percleft: percleft + "px"
    };

    var vis1 = d3.select(config.chart).append("svg:svg")
        .attr("width", width)
        .attr("height", height)
        .append("svg:g")
        .attr("id", "container1")
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
    intializeVisualization(vis1, config).init();
});

d3.json("./data/liabilities.json", function(error, root) {
    if (error) throw error;

    var partition = d3.layout.partition()
        .value(function(d) { return d.size; });

    //var svga = d3.select("#chart1").append("svg")
    //    .attr("width", width)
    //    .attr("height", height + 50)
    //    .append("g")
    //    .attr("class", "liabilities")
    //    .attr("transform", "translate(" + Math.min(width, height) / 2 + "," + (Math.min(width, height) / 2 + 50) + ")");

    //var g = svga.selectAll("g.liabilities")
    //    .data(partition.nodes(root))
    //    .enter().append("g");

    //intializeSunburst(g).init();
});

function intializeVisualization(vis, config) {

    // Total size of all segments; we set this later, after loading the data.
    var totalSize = 0;
    var tWidth = 0;
    var fWidth = 0;

    var arc = d3.svg.arc()
        .startAngle(function(d) { return d.x; })
        .endAngle(function(d) { return d.x + d.dx; })
        .innerRadius(function(d) { return Math.sqrt(d.y); })
        .outerRadius(function(d) { return Math.sqrt(d.y + d.dy); });

    return {
        init: function() {
            createVisualization();
        }
    }

    // Main function to draw and set up the visualization, once we have the data.

    function createVisualization() {

        // Basic setup of page elements.
        initializeBreadcrumbTrail();
        //drawLegend();
        drawLegend2();
        d3.select(config.togglelegend).on("click", toggleLegend);

        // Bounding circle underneath the sunburst, to make it easier to detect
        // when the mouse leaves the parent g.
        vis.append("svg:circle")
            .attr("r", radius)
            .style("opacity", 0);

        // For efficiency, filter nodes to keep only those large enough to see.
        var nodes = partition.nodes(config.json)
            .filter(function(d) {
                return (d.dx > 0.005); // 0.005 radians = 0.29 degrees
            });

        var path = vis.data([config.json])
            .selectAll("path")
            .data(nodes)
            .enter().append("svg:path")
            .attr("display", function(d) { return d.depth ? null : "none"; })
            .attr("d", arc)
            .attr("fill-rule", "evenodd")
            .style("fill", function(d) { return config.colors[d.name]; })
            .style("opacity", 1)
            .on("mouseover", mouseover)
            .on("click", onclick);


        // Add the mouseleave handler to the bounding circle.
        d3.select(config.container).on("mouseleave", mouseleave);

        // Get total size of the tree = value of root node from partition.
        totalSize = path.node().__data__.value;
    };

    function onclick(d) {

        if (d.name === "Equity")
            d3.select(".equity").style("visibility", "");
    }

    // Fade all but the current sequence, and show it in the breadcrumb trail.
    function mouseover(d) {


        var percentage = (100 * d.value / totalSize).toPrecision(3);
        var percentageString = "$" + d.value.toPrecision(2) + "M (" + percentage + "%)";
        if (percentage < 0.1) {
            percentageString = "< 0.1%";
        }
        d3.select(config.chart).select(config.percentage)
            .text(percentageString);
        d3.select(config.chart).select(config.nodename)
            .text(d.name);

        d3.select(config.chart).select(config.explanation)
            .style("top", config.perctop)
            .style("left", config.percleft)
            .style("visibility", "");

        var sequenceArray = getAncestors(d);
        updateBreadcrumbs(sequenceArray, percentageString);

        // Fade all the segments.
        d3.select(config.chart).selectAll("path")
            .style("opacity", 0.3);

        // Then highlight only those that are an ancestor of the current segment.
        d3.select(config.chart).selectAll("path")
            .filter(function(node) {
                return (sequenceArray.indexOf(node) >= 0);
            })
            .style("opacity", 1);

        d3.select(config.sequence)
            .style("visibility", "");
    }

    // Restore everything to full opacity when moving off the visualization.
    function mouseleave(d) {

        // Hide the breadcrumb trail
        var trailId = "#" + config.trail;
        d3.select(config.chart).select(trailId)
            .style("visibility", "hidden");

        // Deactivate all segments during transition.
        d3.select(config.chart).selectAll("path").on("mouseover", null);

        // Transition each segment to full opacity and then reactivate it.
        d3.select(config.chart).selectAll("path")
            .transition()
            .duration(1000)
            .style("opacity", 1)
            .each("end", function() {
                d3.select(this).on("mouseover", mouseover);
            });

        d3.select(config.chart).select(config.explanation)
            .style("top", config.perctop)
            .style("left", config.percleft)
            .style("visibility", "hidden");

        d3.select(config.sequence)
            .style("visibility", "hidden");
    }

    // Given a node in a partition layout, return an array of all of its ancestor
    // nodes, highest first, but excluding the root.
    function getAncestors(node) {
        var path = [];
        var current = node;
        while (current.parent) {
            path.unshift(current);
            current = current.parent;
        }
        return path;
    }

    function initializeBreadcrumbTrail() {
        // Add the svg area.
        var trail = d3.select(config.sequence)
            .append("svg:svg")
            .attr("width", '100%')
            .attr("height", 50)
            .attr("id", config.trail);
        // Add the label at the end, for the percentage.
        trail.append("svg:text")
            .attr("id", config.endlabel)
            .style("fill", "#000");
    }

    // Generate a string that describes the points of a breadcrumb polygon.
    function breadcrumbPoints(d, i) {
        var points = [];
        var w = getWidthOfText(d.name);

        if (w < b.w)
            w = b.w;

        points.push("0,0");
        //points.push(b.w + ",0");
        points.push(w + ",0");
        //points.push(b.w + b.t + "," + (b.h / 2));
        points.push(w + b.t + "," + (b.h / 2));
        //points.push(b.w + "," + b.h);
        points.push(w + "," + b.h);
        points.push("0," + b.h);

        if (i > 0) { // Leftmost breadcrumb; don't include 6th vertex.
            points.push(b.t + "," + (b.h / 2));
        }
        var res = points.join(" ");
        return res;
    }

    // Update the breadcrumb trail to show the current sequence and percentage.
    function updateBreadcrumbs(nodeArray, percentageString) {
        var trialId = "#" + config.trail;
        var endlabelId = "#" + config.endlabel;

        // Data join; key function combines name and depth (= position in sequence).
        var g = d3.select(trialId)
            .selectAll("g")
            .data(nodeArray, function(d) { return d.name + d.depth; });

        // Add breadcrumb and label for entering nodes.
        var entering = g.enter().append("svg:g");

        entering.append("svg:polygon")
            .attr("points", breadcrumbPoints)
            .style("fill", function(d) { return config.colors[d.name]; });


        entering.append("svg:text")
            //.attr("x", (b.w + b.t) / 2)
            .attr("x", function(d) {
                var w = getWidthOfText(d.name);

                if (w < b.w)
                    w = b.w;
                return (w + b.t) / 2;
            })
            .attr("y", b.h / 2)
            .attr("dy", "0.35em")
            .attr("text-anchor", "middle")
            .text(function(d) {

                return d.name;
            });
        fWidth = 0;
        // Set position for entering and updating nodes.
        g.attr("transform", function(d, i) {

            tWidth = 0;

            totalWidth(d);

            if (tWidth < b.w)
                tWidth = b.w;
            fWidth = tWidth + getWidthOfText(d.name);
            return "translate(" + tWidth + ", 0)"; //"translate(" + i * (b.w + b.s) + ", 0)";
        });

        // Remove exiting nodes.
        g.exit().remove();

        // Now move and update the percentage at the end.
        d3.select(trialId).select(endlabelId)
            .attr("x", fWidth + 90) //(nodeArray.length + 0.5) * (b.w + b.s));
            .attr("y", b.h / 2)
            .attr("dy", "0.35em")
            .attr("text-anchor", "middle")
            .text(percentageString);

        // Make the breadcrumb trail visible, if it's hidden.
        d3.select(trialId)
            .style("visibility", "");

    }

    function totalWidth(d) {

        if (d.parent) {
            var w = getWidthOfText(d.parent.name);
            if (w < b.w)
                w = b.w;
            tWidth = tWidth + w + b.s;
            totalWidth(d.parent);
        }

    }

    function drawLegend() {

        // Dimensions of legend item: width, height, spacing, radius of rounded rect.
        var li = {
            w: 75,
            h: 30,
            s: 3,
            r: 3
        };

        for (var i = 0; i < names.length; i++) {

            var w = getWidthOfText(names[i]);
            if (w > li.w)
                li.w = w;
        }
        var legend = d3.select(config.legend).append("svg:svg")
            .attr("width", li.w)
            .attr("height", d3.keys(config.colors).length * (li.h + li.s));

        var g = legend.selectAll("g")
            .data(d3.entries(config.colors))
            .enter().append("svg:g")
            .attr("transform", function(d, i) {
                return "translate(0," + i * (li.h + li.s) + ")";
            });

        g.append("svg:rect")
            .attr("rx", li.r)
            .attr("ry", li.r)
            .attr("width", li.w)
            .attr("height", li.h)
            .style("fill", function(d) { return d.value; });

        g.append("svg:text")
            .attr("x", li.w / 2)
            .attr("y", li.h / 2)
            .attr("dy", "0.35em")
            .attr("text-anchor", "middle")
            .text(function(d) { return d.key; });
    }

    function drawLegend2() {
        var str = "<ul>";

        drawled(config.json.children);

        function drawled(d) {

            for (var i = 0; i < d.length; i++) {

                var clor = config.colors[d[i].name];
                str = str + "<li  ><span class='item' style='background-color:" + clor + ";'>" + d[i].name + "</span>";

                if (d[i].children) {
                    str = str + "<ul>";
                    drawled(d[i].children);
                    str = str + "</ul>";
                }
                str = str + "</li>";
            }
        }

        str = str + "</ul>";

        $(config.legend).html(str);
    }

    function toggleLegend() {
        var legend = d3.select(config.legend);
        if (legend.style("visibility") === "hidden") {
            legend.style("visibility", "");
        } else {
            legend.style("visibility", "hidden");
        }
    }

}

function getNames(d) {

    for (var i = 0; i < d.length; i++) {

        if (names.indexOf(d[i].name) === -1)
            names.push(d[i].name);

        if (d[i].children)
            getNames(d[i].children);
    }

}

function getWidthOfText(txt) {

    var c = document.createElement("canvas");
    var ctx = c.getContext("2d");
    ctx.font = "12px 'Open Sans', sans-serif";
    return ctx.measureText(txt).width + 20;
}

function intializeSunburst(g) {

    var x = d3.scale.linear()
        .range([0, 2 * Math.PI]);

    var y = d3.scale.linear()
        .range([0, radius]);

    var color = d3.scale.category20c();

    var arc = d3.svg.arc()
        .startAngle(function(d) { return Math.max(0, Math.min(2 * Math.PI, x(d.x))); })
        .endAngle(function(d) { return Math.max(0, Math.min(2 * Math.PI, x(d.x + d.dx))); })
        .innerRadius(function(d) { return Math.max(0, y(d.y)); })
        .outerRadius(function(d) { return Math.max(0, y(d.y + d.dy)); });

    return {
        init: function() {
            drawSunburst();
        }
    }


    function drawSunburst() {

        var path = g.append("path")
            .attr("d", arc)
            .style("fill", function(d) { return color((d.children ? d : d.parent).name); })
            .on("click", click);

        var text = g.append("text")
            .attr("transform", function(d) { return "rotate(" + computeTextRotation(d) + ")"; })
            .attr("x", function(d) { return y(d.y); })
            .attr("dx", "6") // margin
            .attr("dy", ".35em") // vertical-align
            .text(function(d) {
                if (d.depth < 3) {
                    return d.name;
                } else {
                    return "";
                }
            })
            .on("click", click);

        function click(d) {
            // fade out all text elements
            text.transition().attr("opacity", 0);

            path.transition()
                .duration(750)
                .attrTween("d", arcTween(d))
                .each("end", function(e, i) {
                    // check if the animated element's data e lies within the visible angle span given in d
                    if (e.x >= d.x && e.x < (d.x + d.dx)) {
                        // get a selection of the associated text element
                        var arcText = d3.select(this.parentNode).select("text");
                        // fade in the text element and recalculate positions
                        arcText.transition().duration(750)
                            .attr("opacity", 1)
                            .attr("transform", function() { return "rotate(" + computeTextRotation(e) + ")" })
                            .attr("x", function(d) { return y(d.y); })
                            .text(function(t) {
                                if (d.parent) {
                                    if ((t.depth - d.parent.depth) < 3) {
                                        return t.name;
                                    } else {
                                        return "";
                                    }
                                } else {
                                    if (t.depth < 3) {
                                        return t.name;
                                    } else {
                                        return "";
                                    }
                                }
                            });

                    }
                });
        }

    }

    // Interpolate the scales!
    function arcTween(d) {
        var xd = d3.interpolate(x.domain(), [d.x, d.x + d.dx]),
            yd = d3.interpolate(y.domain(), [d.y, 1]),
            yr = d3.interpolate(y.range(), [d.y ? 20 : 0, radius]);
        return function(d, i) {
            return i
                ? function(t) { return arc(d); }
                : function(t) {
                    x.domain(xd(t));
                    y.domain(yd(t)).range(yr(t));
                    return arc(d);
                };
        };
    }

    function computeTextRotation(d) {
        return (x(d.x + d.dx / 2) - Math.PI / 2) / Math.PI * 180;
    }

}

$(function() {
    // OPACITY OF BUTTON SET TO 0%
    $(".roll").css("opacity", "0");

    // ON MOUSE OVER
    $(".roll").hover(function() {

        var src = $(this).parent().find('img').attr('src');
        $("#bgimg").attr("src", src);

        // SET OPACITY TO 70%
        $(this).stop().animate({
            opacity: .7
        }, "fast");

        $("#bgimg").stop().animate({
            opacity: 1
        }, "fast");
    },

        // ON MOUSE OUT
        function() {

            // SET OPACITY BACK TO 50%
            $(this).stop().animate({
                opacity: 0
            }, "slow");
            $("#bgimg").stop().animate({
                opacity: 0
            }, "slow");
        });
});

    // Hack to make this example display correctly in an iframe on bl.ocks.org
    d3.select(self.frameElement).style("height", "500px");
